﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;

namespace CaldasPack.Prototyping
{
    public class StringDrawer : MonoBehaviour
    {
        private static readonly List<string> strings = new List<string>();
        private static readonly List<Vector3> positions = new List<Vector3>();
        private static readonly List<Color> colors = new List<Color>();
        private static readonly List<Vector3> offsets = new List<Vector3>();
        private static int stringsToRemove;
        private static StringDrawer instance;
        
        [Conditional("UNITY_EDITOR")]
        public static void Draw(string text, Vector3 position, float offsetX = 0, float offsetY = 0)
        {
            Draw(text, position, Color.black, offsetX, offsetY);
        }

        [Conditional("UNITY_EDITOR")]
        public static void Draw(string text, Vector3 position, Color sceneViewColor, float offsetX = 0, float offsetY = 0)
        {
            if (instance == null)
            {
                var go = new GameObject(nameof(StringDrawer));
                instance = go.AddComponent<StringDrawer>();
                DontDestroyOnLoad(go);
            }

            strings.Add(text);
            positions.Add(position);
            colors.Add(sceneViewColor);
            offsets.Add(new Vector3(offsetX, offsetY));
        }

        private void OnDrawGizmos()
        {
            Handles.BeginGUI();
            for (int i = 0; i < strings.Count; i++)
            {
                GUI.color = colors[i];
                DrawString(strings[i], positions[i], offsets[i]);
            }
            Handles.EndGUI();

            stringsToRemove = strings.Count;
        }
        
        private void DrawString(string text, Vector3 position, Vector3 offset)
        {
            var cam = Camera.current;
            position = cam.ScreenToWorldPoint(cam.WorldToScreenPoint(position) + offset);
            Handles.Label(position, text);
        }

        private void Update()
        {
            CleanOld();
        }

        /// <summary>
        /// OnDrawGizmos() runs more than once per frame in the Editor when there is more than one
        /// Game/Scene window opened. The subsequent runs happens even after OnGUI and WaitForEndOfFrame, so
        /// old strings have to be cleaned in the next frame.
        /// </summary>
        private static void CleanOld()
        {
            strings.RemoveRange(0, stringsToRemove);
            positions.RemoveRange(0, stringsToRemove);
            colors.RemoveRange(0, stringsToRemove);
            offsets.RemoveRange(0, stringsToRemove);
        }
    }
}
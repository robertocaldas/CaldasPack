﻿using UnityEngine;

namespace CaldasPack
{
    /// <summary>
    /// Singleton representation of a MonoBehaviour.
    /// </summary>
    /// <remarks>
    /// This implementation does not prevent the user from having multiple instances of the same class
    /// if they drag and drop it into multiple Game Objects.
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    [DisallowMultipleComponent]
    public class SingletonBehaviour<T> : BaseMonoBehaviour where T : BaseMonoBehaviour
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                if (!instance)
                {
                    instance = FindObjectOfType<T>();

                    if (!instance)
                    {
                        var go = new GameObject(nameof(T));
                        DontDestroyOnLoad(go);
                        instance = go.AddComponent<T>();
                    }
                }

                return instance;
            }
        }
    }
}
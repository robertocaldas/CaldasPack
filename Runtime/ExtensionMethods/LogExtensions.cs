﻿using System.Collections;
using System.Text;

namespace UnityEngine
{
    public static class LogExtensions
    {
        public static string ToPrettyString(this IEnumerable enumerable, bool useNewline = false)
        {
            string separator = useNewline ? "\n" : "; ";
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var obj in enumerable)
            {
                stringBuilder.Append(obj);
                stringBuilder.Append(separator);
            }

            return stringBuilder.ToString();
        }
        
        public static string ToPrettyString(this Vector2 v)
        {
            return $"[{v.x,9:0.0000};{v.y,9:0.0000}]";
        }

        public static string ToPrettyString(this Vector3 v)
        {
            return $"[{v.x,9:0.0000};{v.y,9:0.0000};{v.z,9:0.0000}]";
        }
        
        public static string GetPath(this Component component)
        {
            var path = component.name;
            var t = component.transform;
            while(t.parent)
            {
                t = t.parent;
                path = $"{t.name}/{path}";
            }
            return path;
        }

        public static string GetPath(this GameObject gameObject)
        {
            return gameObject.transform.GetPath();
        }
        
        public static void Log(this object obj)
        {
            Debug.Log(obj);
        }

        public static void LogWarning(this object obj)
        {
            Debug.LogWarning(obj);
        }
    
        public static void LogError(this object obj)
        {
            Debug.LogError(obj);
        }

        public static void Log(this IEnumerable enumerable)
        {
            Debug.Log(enumerable.ToPrettyString());
        }
        public static void LogWarning(this IEnumerable enumerable)
        {
            Debug.LogWarning(enumerable.ToPrettyString());
        }
        public static void LogError(this IEnumerable enumerable)
        {
            Debug.LogError(enumerable.ToPrettyString());
        }

        public static void Log(this string str)
        {
            Debug.Log(str);
        }
        public static void LogWarning(this string str)
        {
            Debug.LogWarning(str);
        }
        public static void LogError(this string str)
        {
            Debug.LogError(str);
        }
    }
}
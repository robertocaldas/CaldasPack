﻿using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace CaldasPack
{
    public static class CommonExtensions
    {
        public static bool IsNullOrEmpty(this ICollection collection)
        {
            return collection == null || collection.Count == 0;
        }

        public static T[] Concat<T>(this T[] array1, T[] array2)
        {
            T[] result = new T[array1.Length + array2.Length];
            array1.CopyTo(result, 0);
            array2.CopyTo(result, array2.Length);
            return result;
        }

        public static T[] RemoveAt<T>(this T[] source, int index)
        {
            T[] destination = new T[source.Length - 1];
            if (index > 0)
                Array.Copy(source, 0, destination, 0, index);

            if (index < source.Length - 1)
                Array.Copy(source, index + 1, destination, index, source.Length - index - 1);

            return destination;
        }

        /// <summary>
        /// Fisher-Yates shuffle algorithm. Performance proportional
        /// to number of elements, does not allocate another IList.
        /// </summary>
        public static void Shuffle(this IList list)
        {
            int n = list.Count;
            while (n > 1)
            {
                var r = Random.Range(0, n);
                n--;
                var obj = list[r];
                list[r] = list[n];
                list[n] = obj;
            }
        }

        public static T RandomElement<T>(this IList<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }
    }
}